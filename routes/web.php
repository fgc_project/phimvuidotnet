<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/test', function () {
    return view('admin.films.list');
});


Route::group(['prefix' => 'admin'], function() {
    Route::group(['middleware' => ['web']], function() {
        Route::resource('post','Backend\CategoryController');
        Route::POST('addPost','Backend\CategoryController@addPost');
        Route::POST('editPost','Backend\CategoryController@editPost');
        Route::POST('deletePost','Backend\CategoryController@deletePost');
    });
});

