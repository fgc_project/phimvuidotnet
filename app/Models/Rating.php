<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table='ratings';

    public function films()
    {
    	return $this->belongsTo('App\Models\Film');
    }

    public function customers()
    {
    	return $this->belongsTo('App\Models\Customer');
    }
}
