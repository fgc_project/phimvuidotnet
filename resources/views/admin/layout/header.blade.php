<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <strong><small>FGC</small></strong></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>FGC MOVIES</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    @include('admin.layout.menu')
</header>