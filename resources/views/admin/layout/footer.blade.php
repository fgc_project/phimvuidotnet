<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.1
    </div>
    <strong>Copyright &copy; <a href="https://www.fgc.vn">FGC TEAM 2018</a>.</strong> All rights
    reserved.
</footer>