@extends('admin.layout.index')


@section('body.content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <small>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Administrator</a></li>
              <li><a href="#">Category</a></li>
              <li class="active">List category</li>
            </ol>
      </small>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            {{-- header --}}
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped" id="table" >
              <thead>
              <tr>
                <th>STT</th>
                <th class="text-center">Category name</th>
                <th class="text-center">Slug</th>
                <th class="text-center">Description</th>
                <th class="text-center">Create at</th>
                <th class="text-center">Update at</th>
                <th width="120" class="text-center">
                  AddCate &nbsp;
                    <a href="#" class="create-modal btn btn-success btn-sm text-justify">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </th>
              </tr>
              </thead>
              <tbody>
              <?php $no = 0; ?>
              @foreach ($category as $cate)
              <?php $no++; ?>
              <tr>
                <td>{{ $no }}</td>
                <td width="120">{{ $cate->name }}</td>
                <td width="120">{{ $cate->slug }}</td>
                <td>{{ $cate->description }}</td>
                <td width="65">{{ $cate->created_at }}</td>
                <td width="65">{{ $cate->updated_at }}</td>
                <td class="text-center">
                    <a href="#" class="show-modal btn btn-info btn-sm" data-slug="{{ $cate->slug }}" data-id="{{ $cate->id }}" data-title="{{ $cate->name }}" data-body="{{ $cate->description }}"   >
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="#" class="edit-modal btn btn-warning btn-sm">
                        <i class="glyphicon glyphicon-pencil"></i>
                    </a>
                    <a href="#" class="delete-modal btn btn-danger btn-sm">
                        <i class="glyphicon glyphicon-trash"></i>
                    </a>
                </td>

              </tr>
              @endforeach
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        {{-- Modal Form Show POST --}}
        <div id="show" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><P class="lead"> </P></h4>
                        </div>
                            <div class="modal-body">
                            <div class="form-group">
                                <label for="">ID : </label>
                                <span  id="i"/>
                            </div>
                            <div class="form-group">
                                <label for="">Title :</label>
                                <span id="ti"/>
                            </div>
                            <div class="form-group">
                                <label for="">Body :</label>
                                <span id="by"/>
                            </div>
                            <div class="form-group">
                                <label for="">Slug :</label>
                                <span id="slug"/>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Modal Form Create Post --}}
        <div id="create" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                  <form class="form-horizontal" role="form">
                    <div class="form-group row add">
                      <label class="control-label col-sm-2" for="title">Title :</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="name"
                        placeholder="Your Title Here" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="body">Body :</label>
                      <div class="col-sm-10">
                        <textarea type="text" class="form-control" id="body" name="description" rows="15" cols="55"
                        placeholder="Your Body Here" required> </textarea>
                        <p class="error text-center alert alert-danger hidden"></p>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" id="add">
                    <span class="glyphicon glyphicon-plus"></span>Save Post
                    </button>
                    <button class="btn btn-warning" type="button" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remobe"></span>Close
                    </button>
                </div>
              </div>
            </div>
          </div>

          
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('body.css')
<link rel="stylesheet" href="public/assets/backend/dist/css/my_list_cate_style.css">
@endsection

@section('body.script')
  <script src="public/assets/backend/dist/js/data_table_paginate.js"></script>

  <script type="text/javascript">
    $(document).on('click','.create-modal',function(e){
        e.preventDefault();
        $("#create").modal('show');
        $(".form-horizontal").show();
        $('.model-title').text('Add Post');
    });

    // show function post
    $(document).on('click', '.show-modal', function(e) {
      e.preventDefault();
      $('#show').modal('show');
      $('#i').text($(this).data('id'));
      $('#ti').text($(this).data('title'));
      $('#by').text($(this).data('body'));
      $('#slug').text($(this).data('slug'));
      $('.modal-title').text('Categories Details');
      });

    // add 
    $("#add").click(function() {
      $.ajax({
        type: 'POST',
        url: 'admin/addPost2',
        data: {
          '_token': $('input[name=_token]').val(),
          'title': $('input[name=title]').val(),
          'body': $('textarea[name=body]').val()
        },
        success: function(data){
          if ((data.errors)) {
            alert(data);
            $('.error').removeClass('hidden');
            $('.error').text(data.errors.title);
            $('.error').text(data.errors.body);
          } else {
            $('.error').remove();
            $('#table').append(
            "<tr class='post" + data.id + "'>"+
            "<td>" + data.id + "</td>"+
            "<td>" + data.title + "</td>"+
            "<td>" + data.body + "</td>"+
            "<td>" + data.created_at + "</td>"+
            "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
            "</tr>");
          }
        },
      });
      $('#title').val('');
      $('#body').val('');
    });
    

    // show function post
  $(document).on('click', '.show-modal', function(e) {
      e.preventDefault();
      $('#show').modal('show');
      $('#i').text($(this).data('id'));
      $('#ti').text($(this).data('title'));
      $('#by').text($(this).data('body'));
      $('.modal-title').text('Show Post');
      });

  // function Edit POST
  $(document).on('click', '.edit-modal', function(e)
      {
          e.preventDefault();
          $('#footer_action_button').text(" Update Post");
          $('#footer_action_button').addClass('glyphicon-check');
          $('#footer_action_button').removeClass('glyphicon-trash');
          $('.actionBtn').addClass('btn-success');
          $('.actionBtn').removeClass('btn-danger');
          $('.actionBtn').addClass('edit');
          $('.modal-title').text('Post Edit');
          $('.deleteContent').hide();
          $('.form-horizontal').show();
          $('#fid').val($(this).data('id'));
          $('#t').val($(this).data('title'));
          $('#b').val($(this).data('body'));
          $('#myModal').modal('show');
      });

  $('.modal-footer').on('click', '.edit', function() {
      $.ajax({
          type: 'POST',
          url: 'admin/editPost',
          data: {
      '_token': $('input[name=_token]').val(),
      'id': $("#fid").val(),
      'title': $('#t').val(),
      'body': $('#b').val()
      //'body': $('textarea[name=body]').val()
          },
      success: function(data) {
          $('.post' + data.id).replaceWith(" "+
          "<tr class='post" + data.id + "'>"+
          "<td>" + data.id + "</td>"+
          "<td>" + data.title + "</td>"+
          "<td>" + data.body + "</td>"+
          "<td>" + data.created_at + "</td>"+
      "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.title + "' data-body='" + data.body + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
          "</tr>");
          }
      });
      });


  // function delete post
  $(document).on('click', '.delete-modal', function(e) {
      e.preventDefault();
      $('#footer_action_button').text(" Delete");
      $('#footer_action_button').removeClass('glyphicon-check');
      $('#footer_action_button').addClass('glyphicon-trash');
      $('.actionBtn').removeClass('btn-success');
      $('.actionBtn').addClass('btn-danger');
      $('.actionBtn').addClass('delete');
      $('.modal-title').text('Delete Post');
      $('.id').text($(this).data('id'));
      $('.deleteContent').show();
      $('.form-horizontal').hide();
      $('.title').html($(this).data('title'));
      $('#myModal').modal('show');
  });

  $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'admin/deletePost',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.post' + $('.id').text()).remove();
        }
      });
  });
      
</script>
@endsection



