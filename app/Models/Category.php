<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function films()
    {
    	return $this->belongsToMany('App\Models\Film');
    }
}
