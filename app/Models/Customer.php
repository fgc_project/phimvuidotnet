<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='customers';

    public function comments()
    {
    	return $this->hasMany('App\Models\Comment');
    }

    public function ratings()
    {
    	return $this->hasOne('App\Models\Rating');
    }
}
