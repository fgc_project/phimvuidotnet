<?php

namespace App\Http\Controllers\Backend;

use App\Models\LinkFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LinkFilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LinkFilm  $linkFilm
     * @return \Illuminate\Http\Response
     */
    public function show(LinkFilm $linkFilm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LinkFilm  $linkFilm
     * @return \Illuminate\Http\Response
     */
    public function edit(LinkFilm $linkFilm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LinkFilm  $linkFilm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LinkFilm $linkFilm)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LinkFilm  $linkFilm
     * @return \Illuminate\Http\Response
     */
    public function destroy(LinkFilm $linkFilm)
    {
        //
    }
}
