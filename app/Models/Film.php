<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table='films';

    public function actors()
    {
        return $this->belongsToMany('App\Modles\Actor');
    }

    public function admin_users()
    {
    	return $this->belongsTo('app\Models\AdminUser');
    }

    public function categories()
    {
    	return $this->belongsToMany('App\Modles\Category');
    }

    public function countries()
    {
    	return $this->belongsToMany('App\Modles\Country');
    }

    public function directors()
    {
    	return $this->belongsToMany('App\Modles\Director');
    }

    public function link_films()
    {
    	return $this->belongsToMany('App\Modles\LinkFilm');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Modles\Tag');
    }

    public function languages()
    {
        return $this->hasMany('App\Models\Language');
    }

    public function ratings()
    {
        return $this->hasMany('App\Models\Rating');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
