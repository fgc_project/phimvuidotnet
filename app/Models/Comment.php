<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table='comments';

    public function customers()
    {
    	return $this->belongsTo('App\Models\Customer');
    }

    public function films()
    {
    	return $this->belongsTo('App\Models\Film');
    }
}
